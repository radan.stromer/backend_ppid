<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	class M_infopublic extends CI_Model {
        function sub_add($data){
            return $this->db->query("
                INSERT INTO info_public_sub(category_info,title) VALUES (?,?)
            ",array($data['category_info'],$data['title']));
        }

        function sub_edit($data){
            return $this->db->query("
               UPDATE info_public_sub SET title=? WHERE id=?
            ",array($data['title'],$data['id']));
        }

        function sub_get($data){
            return $this->db->query("
                SELECT * FROM info_public_sub WHERE id=?
            ",array($data))->row_array();
        }

        function sub_delete($data){
            return $this->db->query("
                DELETE FROM info_public_sub WHERE id=?",array($data));
        }

        function get_item_list($category_info){
            return $this->db->query("
                SELECT 
                    info_public_item.*,
                    info_public_sub.title AS sub_nama
                FROM info_public_item
                LEFT JOIN info_public_sub ON info_public_item.sub_id = info_public_sub.id
                WHERE info_public_sub.category_info = ?
            ",array($category_info))->result_array();
        }

        function item_add($data,$nama_file){
            return $this->db->query("
                INSERT INTO info_public_item(sub_id,title,file) VALUES (?,?,?)
            ",array($data['sub_id'],$data['title'],$nama_file));
        }

        function item_get($data){
            return $this->db->query("
                SELECT * FROM info_public_item WHERE id=?
            ",array($data))->row_array();
        }


        function item_edit($data,$nama_file=''){
            if(!empty($nama_file)) {
                $this->db->query("
                UPDATE info_public_item SET file=? WHERE id=?
                ",array($nama_file,$data['id']));
            }
            return $this->db->query("
               UPDATE info_public_item SET title=? WHERE id=?
            ",array($data['title'],$data['id']));
        }

        function item_delete($id){
            $file = $this->db->query('
                SELECT file FROM info_public_item WHERE id=?
            ',array($id))->row_array();
            unlink('assets/upload/infopublic/'.$file['file']);

            return $this->db->query("
               DELETE FROM info_public_item WHERE id=?
            ",array($id));
        }

        function item_file_delete($id){
            $file = $this->db->query('
                SELECT file FROM info_public_item WHERE id=?
            ',array($id))->row_array();
            unlink('assets/upload/infopublic/'.$file['file']);

            return $this->db->query("
            UPDATE info_public_item SET file='' WHERE id=?
            ",array($id));
        }
    }