<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	class M_pengajuan extends CI_Model {
        function get_list(){
            $query = $this->db->select('pengajuan.*, users.nama as name')
                     ->join('users','pengajuan.user_id=users.id')
                     ->order_by('id', 'DESC')
                     ->get('pengajuan');
            $result	= $query->result_array();
            return $result;
        }

        function get_list_keberatan(){
            $query = $this->db->select('pengajuan.*, users.nama as name')
                     ->join('users','pengajuan.user_id=users.id')
                     ->where('pengajuan.status','Keberatan sedang direview')
                     ->order_by('pengajuan.id', 'DESC')
                     ->get('pengajuan');
            $result	= $query->result_array();
            return $result;
        }

        function get_detail($id){
            $sql = "SELECT pengajuan.*, users.nama, users.email, users.telp, users.nik
                    FROM pengajuan
                    JOIN users ON pengajuan.user_id=users.id
                    WHERE pengajuan.id = ? ";
            return $this->db->query($sql, array($id))->row_array(); 
        }

        function get_last_no_pengajuan()		
		{		
			$query = $this->db->select('*')
					 ->limit(1)
					 ->where('waktu >',date("Y-01-01 00:00:00"))
					 ->where('waktu <',date("Y-12-t 23:59:59"))
					 ->order_by('id', 'DESC')
					 ->get('pengajuan');
            $result	= $query->row_array();
            
            if(empty($result)){
				$number_pengajuan = "1";
            }else{
                $number_pengajuan = explode("/",$result['no_pengajuan']);
                $number_pengajuan = (int)$number_pengajuan[0]+1;
            }
            return $number_pengajuan."/PPID.BP2MI/".integerToRoman(date('m'))."/".date('Y');
        }

        function dijawab($data, $id){ 
            $sql = "UPDATE pengajuan 
                        SET jawaban=?,
                            link_lampiran=?,
                            status='Sudah dijawab'
                        WHERE id=?
                    ";
            return $this->db->query($sql,array(
                        $data['jawaban'],
                        $data['link_lampiran'],
                        $id
            ));
           
        }

        function keberatan_dijawab($data, $id){
            $sql = "UPDATE pengajuan 
                        SET keberatan_jwb=?,
                            status='Keberatan sudah dijawab'
                        WHERE id=?
                    ";
            return $this->db->query($sql,array(
                        $data['jawaban-keberatan'],
                        $id
            ));
           
        }

        function update_selesai($id){ 
            $sql = "UPDATE pengajuan 
                        SET status='Sudah selesai'
                        WHERE id=?
                    ";
            return $this->db->query($sql,array($id));
           
        }
    } 