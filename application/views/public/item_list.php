<?php $this->load->view('include/header');?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Item</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <a href="<?=base_url('Infopublic/item_form/'.$sub_id);?>" class="btn btn-info">+ Tambah informasi</a>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Judul dokumen</th>
                      <th>Sub bagian</th>
                      <th>Tindakan</th>
                    </tr>
                  </thead>
                  <?php foreach($data as $v){?>
                    <tr>
                      <td><?=$v['title'];?></td>
                      <td><?=$v['sub_nama'];?></td>
                      <td><a style="padding:5px; font-size:10px;" class="btn btn-primary" href="<?=base_url('Infopublic/item_form/'.$sub_id.'/'.$v['id']);?>">Edit</a>
                      <a onclick="return confirm('Anda yakin ingin menghapus?')" class="btn btn-danger btn-hapus btn-sm btn-circle" href="<?=base_url('Infopublic/item_hapus/'.$v['id'].'/'.$sub_id);?>"><i class="fas fa-trash"></a></td>
                    </tr>
                  <?php }?>
                  <tfoot>
                    <tr>
                      <th>Judul dokumen</th>
                      <th>Sub bagian</th>
                      <th>Tindakan</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php $this->load->view('include/footer');?>
