
<?php $this->load->view('include/header');?>
<div class="card shadow mb-4">
    <div class="card-body">
        <h3><?=(!empty($id))?'Edit':'Tambah ';?> Informasi</h3>
        <form method="post" enctype="multipart/form-data">
            <?php $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
            ?>
            <?php if(!empty($id)){?>
                <input type="hidden" name="id" value="<?=$id;?>">
            <?php }?>
            <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <input type="hidden" name="sub_id" value="<?=$sub_id;?>">

            <div class="form-group">
                <label>Sub bagian</label>
                <select name="sub_id" class="form-control">
                    <?php foreach($sub as $v){?>
                        <option value="<?=$v['id']?>"><?=$v['title'];?></option>
                    <?php }?>
                </select>
            </div>

            <div class="form-group">
                <label>Title</label>
                <input type="text" name="title" value="<?=(!empty($title))?$title:'';?>" class="form-control"/>
            </div>
            <div class="form-group">
                <label>File</label>
                <?php if(empty($file)){?>
                    <input type="file" name="file_dokumen" class="form-control"/>
                <?php }else{?>
                    <a href="<?=base_url('assets/upload/infopublic/'.$file);?>">Link Preview File</a> |
                    <a style="color:red;" href="<?=base_url('infopublic/item_file_hapus/'.$id.'/'.$red);?>" onClick="return confirm('Are you sure?')">Hapus file</a>
                <?php }?>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
<?php $this->load->view('include/footer');?>