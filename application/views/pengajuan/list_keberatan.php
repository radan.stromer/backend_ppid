<?php $this->load->view('include/header');?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Keberatan PPID BP2MI</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr style="background:#16529b; text-align:center; color:white;">
                      <th style="vertical-align: middle;">No</th>
                      <th style="vertical-align: middle;">Tanggal</th>
                      <th style="vertical-align: middle;">Pemohon</th>
                      <th style=" vertical-align: middle; width:21%;">Rincian Keberatan</th>
                      <th style="vertical-align: middle;">Status</th>
                      <th style="vertical-align: middle;">Tindakan</th>
                    </tr>
                  </thead>
                  <?php $no=1; foreach ($list_pengajuan as $key => $v){?>
                    <tr >
                      <td style=" text-align:center;"><?=$no;?></td>
                      <td  style="width:84%; text-align:center;"><?=$v['last_edit'];?></td>
                      <td><?=$v['name'];?></td>
                      <td><?=$v['keberatan'];?></td>
                      <td><strong><?=$v['status'];?></strong></td>
                      <td style="font-weight: bold; color: orange; text-align:center;"><a style="padding:2px; font-size:10px;" class="btn btn-primary" href="<?=base_url('pengajuan/detail/'.$v['id']);?>">Detail</a>
                      <?=($v['status']=='Keberatan sedang direview')?batas_waktu_keberatan($v['last_edit']):'';?>
                      </td>
                    </tr>
                  <?php $no++;}?>
                  <tfoot>
                    <tr style="background:#16529b; text-align:center; color:white;">
                      <th style="vertical-align: middle;">No</th>
                      <th style="vertical-align: middle;">Tanggal</th>
                      <th style="vertical-align: middle;">Pemohon</th>
                      <th style=" vertical-align: middle; width:21%;">Rincian Keberatan</th>
                      <th style="vertical-align: middle;">Status</th>
                      <th style="vertical-align: middle;">Tindakan</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php $this->load->view('include/footer');?>