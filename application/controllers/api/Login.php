<?php
    require APPPATH . 'third_party/RestController.php';
    require APPPATH . 'third_party/Format.php';

    use chriskacerguis\RestServer\RestController;

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
    class Login extends RestController {
        function  __construct(){
            parent::__construct();
            // Load these helper to create JWT tokens
            $this->load->helper(['jwt', 'authorization']);
        }

        public function hello_get(){
            $tokenData = 'Hello World!';
            
            // Create a token
            $token = AUTHORIZATION::generateToken($tokenData);
            // Set HTTP status code
            $status = parent::HTTP_OK;
            // Prepare the response
            $response = ['status' => $status, 'token' => $token];
            // REST_Controller provide this method to send responses
            $this->response($response, $status);
        }

        
        public function login_post(){
            $this->load->model('M_user');
            $user = $this->M_user->cek_login($this->post());

            if(!empty($user)){
                // Create a token from the user data and send it as reponse
                $token = AUTHORIZATION::generateToken(['user_email' => $user['email']]);
                // Prepare the response
                $status = parent::HTTP_OK;
                $response = ['status' => $status, 'token' => $token];
                $this->response($response, $status);
            }else{
                $this->response(['msg' => 'Invalid username or password!'], parent::HTTP_NOT_FOUND); 
            }
        }

        private function verify_request(){
            $headers = $this->input->request_headers();
            $token   = $headers['Authorization'];
            try {
                // Validate the token
                // Successfull validation will return the decoded user data else returns false
                $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                    $this->response($response, $status);
                    exit();
                } else {
                    return $data;
                }
            } catch (Exception $e) {
                // Token is invalid
                // Send the unathorized access message
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
        
            }
        }
}