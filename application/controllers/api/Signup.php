<?php
    require APPPATH . 'third_party/RestController.php';
    require APPPATH . 'third_party/Format.php';

    use chriskacerguis\RestServer\RestController;

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    class Signup extends RestController {
        function  __construct(){
            parent::__construct();
            $this->load->helper(['jwt', 'authorization']);
        }

        public function signup_post(){
            
            // $response = ['status' => '200', 'msg' =>'Anda berhasil terdaftar.'];
            // $this->response($response, 200);
            $obj        = $this->post();
            $nama       = $obj['nama'];
            $email      = $obj['email'];
            $password   = $obj['password'];
            $nik        = $obj['nik'];
            $pekerjaan  = $obj['pekerjaan'];
            $alamat     = $obj['alamat'];
            $telp       = $obj['telp'];

            $this->load->model('M_user');
            $CheckSQL = $this->M_user->cek_email_exist($email);
            
            if($CheckSQL==1){
                 $respon['code'] = '201';
                 $respon['msg']  = 'Email telah terdaftar !!!';
                 $respon_json = json_encode($respon);
                $status = parent::HTTP_OK;
                $response = ['status' => $status, $respon];
                $this->response($response, $status);
            }else{
                $i = $this->M_user->insert_user($obj);
            
                if($i){
            
                    // If the record inserted successfully then show the message.
                      $respon['code'] = '200';
                      $respon['msg']  = 'Anda berhasil terdaftar.';
                    
                    // // Converting the message into JSON format.
                    $respon_json = json_encode($respon);

                  // $status = parent::HTTP_OK;
                    $response = ['status' => '200', 'msg' =>'Anda berhasil terdaftar.'];
                    $this->response($response, 200);
                }else{
                    //$respon['code'] = '200';
                    //$respon['msg']  = 'Pendaftaran gagal, silahkan coba lagi.';

                    $status = parent::HTTP_OK;
                    $response = ['status' => $status, 'msg' => 'Pendaftaran gagal, silahkan coba lagi.'];
                    $this->response($response, $status);
                
                }
            }

            //echo $respon_json ;
        }
}