<?php

class Login extends CI_Controller {
    function  __construct(){
        parent::__construct();
        
    }
	public function index()
	{
		$data = array();
	    if($_POST){
			$this->load->helper('security');
			$user_password = do_hash($this->input->post('pass'), 'md5');
			
			$this->load->model('M_user');
			$r = $this->M_user->cek_login_admin($this->input->post('email'),$user_password);

			if($r){
				$_SESSION['admin_id'] 		 = $r['id'];
				$_SESSION['admin_email'] 	 = $r['email'];
				$_SESSION['admin_name'] 	 = $r['name'];
				$_SESSION['admin_level'] 	 = $r['level'];
				if($r['level']==2)redirect('pengajuan');
				else redirect('pengajuan');
			}else{
				$data['msg'] = 'User dan password salah';
				//$this->session->set_flashdata('alert', 'User dan password salah');
				///redirect('login');
			}
		}
		$this->load->view('login',$data);
	}
	public function logout(){
	    session_destroy();
	    redirect('login');
	}
}
