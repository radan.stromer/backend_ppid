<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    function  __construct(){
        parent::__construct();
        if(empty($_SESSION['admin_id']) AND $_SESSION['admin_level']!=1){
            redirect('/login');
        }
    }
	public function index()
	{
	    $data['admin'] = $this->m_crud->get_list_one_where('admin','level','2','id','asc');
		$this->load->view('admin/list',$data);
	}

	public function add(){
	    if($_POST){
	        $this->load->helper('security');
	        $_POST['pass'] = do_hash($_POST['pass'], 'md5');
	        $_POST['level'] = 2;
	        $this->m_crud->add('admin',$_POST);
	        redirect('admin');
	    }
		$this->load->view('admin/form');
	}
	
	public function hapus($id){
	    $this->m_crud->delete('admin','id',$id);
	    redirect('admin');
	}
}
