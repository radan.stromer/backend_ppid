<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends CI_Controller {
    function  __construct(){
        parent::__construct();
        if(empty($_SESSION['admin_id'])){
            redirect('login');
        }
    }
	public function index()
	{
		$this->load->model('M_pengajuan');
		$data['list_pengajuan'] = $this->M_pengajuan->get_list();
		$this->load->view('pengajuan/list',$data);
	}
	
	public function keberatan()
	{
		$this->load->model('M_pengajuan');
		$data['list_pengajuan'] = $this->M_pengajuan->get_list_keberatan();
		$this->load->view('pengajuan/list_keberatan',$data);
	}

	public function detail($id){
		$this->load->model('M_pengajuan');
		$data['pengajuan'] = $this->M_pengajuan->get_detail($id);
		if($data['pengajuan']['status']=='Keberatan sedang direview')$data['action_form'] = base_url('pengajuan/keberatan_dijawab/'.$data['pengajuan']['id']);
		else $data['action_form'] = base_url('pengajuan/dijawab/'.$data['pengajuan']['id']);
		$this->load->view('pengajuan/detail',$data);
	}

	public function dijawab($id){
		$this->load->model('M_pengajuan');
		$this->M_pengajuan->dijawab($this->input->post(), $id);
		redirect('pengajuan/detail/'.$id);
	}

	public function keberatan_dijawab($id){
		$this->load->model('M_pengajuan');
		$this->M_pengajuan->keberatan_dijawab($this->input->post(), $id);
		redirect('pengajuan/detail/'.$id);
	}

	public function hapus($id){
		$this->m_crud->delete('pengajuan','id',$id);
		redirect('pengajuan');
	}
}
