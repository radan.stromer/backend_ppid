-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 10, 2020 at 07:00 PM
-- Server version: 10.2.30-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `niqsymedia_bp2mi`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(350) NOT NULL,
  `pass` text NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `pass`, `level`) VALUES
(1, 'Kabag Humas', 'ahnas@bnp2tki.go.id', '0192023a7bbd73250516f069df18b500', 1),
(6, 'Kasubag Publikasi Dokumentasi dan Perustakaan', 'muhamad.hapipi@gmail.com', '0192023a7bbd73250516f069df18b500', 2),
(7, 'Staf Publikasi Dokumentasi dan Perpustakaan', 'lintang.dinarandari@bnp2tki.go.id', '0192023a7bbd73250516f069df18b500', 2),
(8, 'admin', 'admin@bnp2tki.go.id', '21232f297a57a5a743894a0e4a801fc3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan`
--

CREATE TABLE `pengajuan` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `no_pengajuan` varchar(45) NOT NULL,
  `rincian` text NOT NULL,
  `tujuan` text NOT NULL,
  `cara_peroleh_info` varchar(45) NOT NULL,
  `cara_dapat_salinan` varchar(45) NOT NULL,
  `status` enum('Sudah dijawab','Keberatan sudah dijawab','sedang direview','Keberatan sedang direview') DEFAULT 'sedang direview',
  `waktu` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `keberatan` text DEFAULT NULL,
  `alasan_keberatan` text NOT NULL,
  `info_keberatan` text NOT NULL,
  `keberatan_jwb` text DEFAULT NULL,
  `file_ktp` text DEFAULT NULL,
  `file_dokumen` text DEFAULT NULL,
  `jawaban` text NOT NULL,
  `link_lampiran` text NOT NULL,
  `admin_last_edit` int(11) DEFAULT NULL,
  `last_edit` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajuan`
--

INSERT INTO `pengajuan` (`id`, `user_id`, `no_pengajuan`, `rincian`, `tujuan`, `cara_peroleh_info`, `cara_dapat_salinan`, `status`, `waktu`, `keberatan`, `alasan_keberatan`, `info_keberatan`, `keberatan_jwb`, `file_ktp`, `file_dokumen`, `jawaban`, `link_lampiran`, `admin_last_edit`, `last_edit`) VALUES
(12, 42, '18/PPID.BP2MI/IV/2020', 'Xjdjd', 'Djdjd', 'Mendapatkan Salinan', 'Faksimili', 'Keberatan sudah dijawab', '2020-04-10 11:50:21', 'test isi rincian keberatan', 'test alasan keberatan', 'test info keberatan', 'test jawab kebertan', 'ktp-12.png', 'doc-12.png', 'test awab', '#', NULL, '2020-04-10 11:50:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` text NOT NULL,
  `alamat` text NOT NULL,
  `nik` varchar(30) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `telp` varchar(50) NOT NULL,
  `status` enum('0','1','2','') NOT NULL DEFAULT '0' COMMENT '0) not verified 1) active 2) not active',
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `by_admin` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `password`, `alamat`, `nik`, `pekerjaan`, `telp`, `status`, `create_date`, `update_date`, `by_admin`) VALUES
(42, 'dadan test', 'radan.stromer@gmail.com', '$2y$10$XLG2/pFO4df61Gh/eBkaP.ALYQlfHHkR41nKXUN69WrkKm1/gRoDq', 'Dhdhdj', '598686', 'Gdgd', '688656', '1', '2020-03-21 06:05:22', '2020-03-31 18:35:51', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengajuan`
--
ALTER TABLE `pengajuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pengajuan`
--
ALTER TABLE `pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
